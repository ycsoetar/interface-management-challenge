###WATonomous Coding Challenge
#####Software Interface Management

This program complains to the software interface management lead about this coding challenge :). The files are split among source files and header files, in src and include respectively. You can run the bash script named "script" in order to lint the code, make the code, and run the program executable.

#####Why Devops is Important for WATonomous:

Devops is crucial for any software related team, especially those that are made up of a lot of members. Hence, having a really solid Devops team for WATonomous is definitely important to help maintain the WATonomous codebase that is edited by so many different students. Having a team that is dedicated to ensure that the quality of the code written throughout the different teams is maintained can go a long way for a design team. Continuous integration allows for automated builds and automated testing, and as a result we are able to more efficiently keep the enormous code base that WATonomous has more up to date and clean.   
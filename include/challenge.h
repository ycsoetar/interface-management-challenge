// Copyright [2018] <Yvan Soetarman>
#ifndef DESKTOP_WATONCHALLENGE_INTERFACE_MANAGEMENT_CHALLENGE_INCLUDE_CHALLENGE_H_
#define DESKTOP_WATONCHALLENGE_INTERFACE_MANAGEMENT_CHALLENGE_INCLUDE_CHALLENGE_H_
#include <string>
#include <iostream>

class waton {
    std::string subteam;
    std::string teamLead;
    std::string action;
    bool happy;
 public:
    waton(std::string subteam, std::string teamLead, std::string action,
        bool happy);
    ~waton();
    std::string status();
};
#endif  // DESKTOP_WATONCHALLENGE_INTERFACE_MANAGEMENT_CHALLENGE_INCLUDE_CHALLENGE_H_

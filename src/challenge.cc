// Copyright [2018] <Yvan Soetarman>
#include "../include/challenge.h"

using std::string;

waton::waton(string subteam, string teamLead, string action, bool happy):
    subteam(subteam), teamLead(teamLead), action(action), happy(happy) {
}

waton::~waton() {}
string waton::status() {
    string status;
    if (happy) {
        status = teamLead + "is the best! " + subteam + " is really great!";
        status += "Right now we have to " + action + ".";
    } else {
        status = "I can't BELIEVE " + teamLead + " is making me " + action;
        status += " for " + subteam + " :|";
    }
    return status;
}
